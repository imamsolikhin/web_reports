<?php
include dirname(__FILE__)."/PHPJasperXML.inc.php";
include dirname(__FILE__)."/setting.php";;
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

$filename = isset($_GET['filename']) ? $_GET['filename'] : "packing-commissions-global";
$title = (isset($_GET['payment_type'])) ? strtoupper($_GET['payment_type']):"BANK RECEIVED";
$company_id = isset($_GET['company_id']) ? ' AND sls_sales_order.company_id = "'.$_GET['company_id'].'"' : "";
$closing_period_id = isset($_GET['closing_period_id']) ? ' AND sls_sales_order.closing_by = "'.$_GET['closing_period_id'].'"' : "";
$closing_period = isset($_GET['closing_period']) ? $_GET['closing_period'] : " PERIODE ".date("d/m/Y",strtotime($_GET['from_date']))." to ".date("d/m/Y",strtotime($_GET['to_date']));
$transaction_status = isset($_GET['transaction_status']) ? ' AND sls_sales_order.confirm_status = "'.$_GET['transaction_status'].'"' : ' AND sls_sales_order.confirm_status != "Cancel"';
$payment_type = isset($_GET['payment_type']) ? ' AND mst_payment_type.name = "'.$_GET['payment_type'].'"' : '';
$seller_id = isset($_GET['seller_id']) ? ' AND sls_sales_order_detail.status = '.$_GET['seller_id'] : '';
$seller_name = isset($_GET['seller_name']) ? ' - '.$_GET['seller_name'] : '';
$from_date = isset($_GET['from_date']) ? ' AND sls_sales_order_closing.closing_date >= "'.$_GET['from_date'].'"' : "";
$to_date = isset($_GET['to_date']) ? ' AND sls_sales_order_closing.closing_date <= "'.$_GET['to_date'].'"' : "";
if($closing_period_id != ""){
  $from_date = $closing_period_id;
  $to_date = "";
}

$query = '
          SELECT
              sls_sales_order.*,
              IF( sls_sales_order_detail.status, "YES", "NO") AS seller,
              "1" as packet,
              DATE_FORMAT(sls_sales_order.transaction_date , "%d/%m/%Y %H:%i") AS transaction_date,
              CONCAT(DATE_FORMAT(sls_sales_order_closing.closing_date, "%d/%m/%Y")) AS transaction_date_temp,
              CONCAT(IFNULL( mst_gender.NAME,"")," ",mst_customer.full_name) AS full_name,
              mst_customer.phone AS phone,
              mst_gender.id AS gender_id,
              mst_advertise.name AS advertise_name,
              CONCAT(mst_company.name,"'.$seller_name.'") AS company_name,
              IFNULL( mst_gender.NAME, "" ) AS gender_name,
              IFNULL( mst_bank.NAME, "" ) AS bank_name,
              IFNULL( mst_market.NAME, "" ) AS market_name,
              IFNULL( mst_courier.NAME, "" ) AS courier_name,
              IFNULL( mst_payment_type.NAME, "" ) AS payment_type_name,
              IFNULL( SUM( sls_sales_order_detail.quantity ), 0 ) AS quantity,
              IFNULL( SUM( sls_sales_order_detail.price ), 0 ) AS price,
              IFNULL( SUM( sls_sales_order_detail.voucer ), 0 ) AS voucer,
              IFNULL(sls_sales_order.cost_handler,0) + ((IFNULL( SUM( sls_sales_order_detail.quantity ), 0 ) * IFNULL( SUM( sls_sales_order_detail.price ), 0 ))-IFNULL( SUM( sls_sales_order_detail.voucer ), 0 ) - IFNULL( SUM( sls_sales_order_closing_detail.balance_price ), 0 )) as cost_handler,
              IFNULL(SUM(sls_sales_order_closing_detail.balance_price),(IFNULL(SUM(sls_sales_order_detail.quantity),0)*IFNULL(SUM(sls_sales_order_detail.price),0))-IFNULL(SUM(sls_sales_order_detail.voucer),0)) AS total_transaction,
              SUM(IF((IFNULL(sls_sales_order_detail.status,0) = 1 OR sls_sales_order_detail.status <> "") ,mst_company.commission_price,IFNULL(mst_commission_rate.commission_price,mst_company.commission_price))*sls_sales_order_detail.quantity) as sum_commission,
              IFNULL( mst_customer_address.address, "" ) AS address,
              IFNULL( mst_customer_address.address_no, "" ) AS address_no,
              IFNULL( mst_customer_address.rt, "" ) AS rt,
              IFNULL( mst_customer_address.rw, "" ) AS rw,
              IFNULL( mst_customer_address.village, "" ) AS village,
              IFNULL( mst_customer_address.sub_district, "" ) AS sub_district,
              IFNULL( mst_customer_address.benchmark, "" ) AS benchmark,
              IFNULL( mst_customer_address.city_id, "" ) AS city_id,
              IFNULL( mst_customer_address.district, "" ) AS district,
              IFNULL( mst_customer_address.province_id, "" ) AS province_id,
              IFNULL( mst_customer_address.postal_code, "" ) AS postal_code,
              IFNULL( users.name, "No Packing" ) AS packing_name
          FROM
              sls_sales_order_closing
              INNER JOIN sls_sales_order_closing_detail ON sls_sales_order_closing_detail.sales_order_closing_id = sls_sales_order_closing.id
              INNER JOIN sls_sales_order ON sls_sales_order.id = sls_sales_order_closing_detail.sales_order_id
              INNER JOIN sls_sales_order_detail ON sls_sales_order_detail.sales_order_id = sls_sales_order.id
              LEFT JOIN mst_commission_rate ON mst_commission_rate.sales_id = sls_sales_order.sales_id
                  AND sls_sales_order.transaction_date >= mst_commission_rate.start_date
                  AND sls_sales_order.transaction_date <= mst_commission_rate.end_date
              LEFT JOIN mst_bank ON mst_bank.id = sls_sales_order.bank_id
              LEFT JOIN mst_market ON mst_market.id = sls_sales_order.market_id
              LEFT JOIN mst_courier ON mst_courier.id = sls_sales_order.courier_id
              LEFT JOIN mst_payment_type ON mst_payment_type.id = sls_sales_order.payment_type_id
              LEFT JOIN mst_customer ON mst_customer.id = sls_sales_order.customer_id
              LEFT JOIN mst_company ON mst_company.id = sls_sales_order.company_id
              LEFT JOIN mst_customer_address ON mst_customer_address.id = sls_sales_order.customer_address_id
              LEFT JOIN mst_advertise ON mst_advertise.id = sls_sales_order.advertise_id
              LEFT JOIN mst_gender ON mst_gender.id = mst_customer.gender_id
              LEFT JOIN users ON users.id = sls_sales_order.packing_id
          WHERE sls_sales_order.id IS NOT NULL
              '
              .$company_id
              .$from_date
              .$to_date
              .$seller_id
              .'
          GROUP BY
              sls_sales_order.id,
              sls_sales_order_closing.id,
              sls_sales_order_detail.status
          ORDER BY
              sls_sales_order.company_id ASC,
              sls_sales_order.bank_id ASC,
              sls_sales_order_closing.closing_date ASC,
              sls_sales_order.transaction_date ASC,
              sls_sales_order.author ASC,
              sls_sales_order.confirm_status ASC
        ';
$result = mysqli_query($conn, $query);
$data = mysqli_fetch_all ($result, MYSQLI_ASSOC);

$PHPJasperXML = new PHPJasperXML();
// $PHPJasperXML = new PHPJasperXML("en","XLS");
$PHPJasperXML->arrayParameter=array(
   "url_logo"=>"http://".$host."/gudangdunia_l.png"
  ,"title"=>"REPORT SALES ORDER BY BANK"
  ,"sub_title"=>"sub report"
  ,"periode_start"=>date("d/m/yy H:i",strtotime($_GET['from_date']))
  ,"periode_end"=>date("d/m/yy H:i",strtotime($_GET['to_date']))
);

$PHPJasperXML->load_xml_file("sales-order-closing-bank-detail.jrxml");
$PHPJasperXML->arraysqltable=$data;
$PHPJasperXML->outpage("I","reports-sales-order-closing-bank-detail.pdf"); //I||D||F
?>
