<?php
include dirname(__FILE__)."/PHPJasperXML.inc.php";
include dirname(__FILE__)."/setting.php";;
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

$filename = isset($_GET['filename']) ? $_GET['filename'] : "packing-commissions-global";
$company_id = isset($_GET['company_id']) ? ' AND sls_sales_order.company_id = "'.$_GET['company_id'].'"' : "";
$closing_period_id = isset($_GET['closing_period_id']) ? ' AND sls_sales_order.closing_by = "'.$_GET['closing_period_id'].'"' : "";
$closing_period = isset($_GET['closing_period']) ? $_GET['closing_period'] : " PERIODE ".date("d/m/Y",strtotime($_GET['from_date']))." to ".date("d/m/Y",strtotime($_GET['to_date']));
$transaction_status = isset($_GET['transaction_status']) ? ' AND sls_sales_order.confirm_status = "'.$_GET['transaction_status'].'"' : ' AND sls_sales_order.confirm_status != "Cancel"';
$payment_type = isset($_GET['payment_type']) ? ' AND mst_payment_type.name = "'.$_GET['payment_type'].'"' : '';
$from_date = isset($_GET['from_date']) ? ' AND sls_sales_order.transaction_date >= "'.$_GET['from_date'].'"' : "";
$to_date = isset($_GET['to_date']) ? ' AND sls_sales_order.transaction_date <= "'.$_GET['to_date'].'"' : "";
if($closing_period_id != ""){
  $from_date = $closing_period_id;
  $to_date = "";
}

$query = '
          SELECT
              users.name AS author,
              mst_company.id AS company_id,
              mst_company.name AS company_name,
              IFNULL( COUNT( sls_sales_order.id ), 0 ) AS packet,
              IFNULL( SUM( sls_sales_order_detail.quantity ), 0 ) AS quantity,
              IFNULL( SUM( sls_sales_order_detail.price ), 0 ) AS price,
              IFNULL( SUM( sls_sales_order_detail.voucer ), 0 ) AS voucher_total,
              IFNULL(sls_sales_order.insurance,0) as insurance,
              IFNULL(sls_sales_order.cost_handler,0) + ((IFNULL( SUM( sls_sales_order_detail.quantity ), 0 ) * IFNULL( SUM( sls_sales_order_detail.price ), 0 ))-IFNULL( SUM( sls_sales_order_detail.voucer ), 0 ) - IFNULL( SUM( sls_sales_order_closing_detail.balance_price ), 0 )) as cost_handler,
              IFNULL(sls_sales_order.transaction,0) as total_transaction,
              IFNULL(SUM(sls_sales_order_closing_detail.balance_price),(IFNULL(SUM(sls_sales_order_detail.quantity),0)*IFNULL(SUM(sls_sales_order_detail.price),0))-IFNULL(SUM(sls_sales_order_detail.voucer),0)) AS total_transaction,
              COUNT(sls_sales_order.id)*1000 AS sum_commission
          FROM
              sls_sales_order
              LEFT JOIN sls_sales_order_detail ON sls_sales_order_detail.sales_order_id = sls_sales_order.id
              LEFT JOIN sls_sales_order_closing_detail ON sls_sales_order_closing_detail.sales_order_id = sls_sales_order.id
              LEFT JOIN sls_sales_order_closing ON sls_sales_order_closing.id = sls_sales_order_closing_detail.sales_order_closing_id
              LEFT JOIN mst_commission_rate ON mst_commission_rate.sales_id = sls_sales_order.sales_id
                  AND sls_sales_order.transaction_date >= mst_commission_rate.start_date
                  AND sls_sales_order.transaction_date <= mst_commission_rate.end_date
              LEFT JOIN mst_company ON mst_company.id = sls_sales_order.company_id
              LEFT JOIN users ON users.id = sls_sales_order.packing_id
          WHERE 1 = 1
              '
              .$company_id
              .$from_date
              .$to_date
              .$transaction_status
              .'
          GROUP BY
              sls_sales_order.company_id,
              sls_sales_order.packing_id
          ORDER BY
              sls_sales_order.company_id ASC,
              sls_sales_order.packing_id ASC
        ';
// var_dump($query);die;
// var_dump($result);die;
$result = mysqli_query($conn, $query);
// var_dump($query);die;
$data = mysqli_fetch_all ($result, MYSQLI_ASSOC);

$PHPJasperXML = new PHPJasperXML();
// $PHPJasperXML = new PHPJasperXML("en","XLS");
$PHPJasperXML->arrayParameter=array(
   "url_logo"=>"http://".$host."/gudangdunia_l.png"
  ,"title"=>"REPORT PACKING COMMISSIONS DETAIL"
  ,"sub_title"=>strtoupper($closing_period)
  ,"periode_start"=>date("d/m/yy H:i",strtotime($_GET['from_date']))
  ,"periode_end"=>date("d/m/yy H:i",strtotime($_GET['to_date']))
);

$PHPJasperXML->load_xml_file("packing-order-commissions.jrxml");
$PHPJasperXML->arraysqltable=$data;
$PHPJasperXML->outpage("I","reports-packing-commissions.pdf"); //I||D||F
?>
